# MongoDB101 - Operaciones en la Base de Datos MongoDB

Este README documenta varias operaciones realizadas en una base de datos MongoDB llamada "grades". A continuación, se presentan las operaciones y sus resultados:
javascript
## Conteo de Documentos
```javascript
> db.grades.count()
800
Se han contado un total de 800 documentos en la colección "grades".
```
## Búsqueda de Documentos por "student_id"
```javascript
> db.grades.find({"student_id": 4})
Se han encontrado varios documentos relacionados con el estudiante con "student_id" 4. Aquí se muestran algunos ejemplos de los documentos encontrados, incluyendo el tipo de evaluación y la puntuación.
```
## Conteo de Documentos por Tipo
```javascript
> db.grades.countDocuments({ type: "exam" })
200
> db.grades.countDocuments({ type: "homework" })
400
> db.grades.countDocuments({ type: "quiz" })
200
Se ha contado la cantidad de documentos de diferentes tipos (exam, homework, quiz) en la colección "grades".
```
## Eliminación de Documentos
```javascript
> db.grades.deleteMany({student_id: 3})
Se han eliminado todos los documentos relacionados con el estudiante con "student_id" 3.
```

## Búsqueda de Documentos por Puntuación
```javascript
> db.grades.find({score: 75.29561445722392})
Se ha encontrado un documento con una puntuación específica (75.29561445722392) en la colección "grades".
```

## Actualización de Documento
```javascript
> db.grades.updateOne({ "_id": ObjectId("50906d7fa3c412bb040eb591") }, { $set: { "score": 100 } })
Se ha actualizado la puntuación de un documento específico con el UUID "50906d7fa3c412bb040eb591" y se ha establecido en 100.
```

## Búsqueda de Estudiante por Documento
```javascript
> db.grades.findOne({ "_id": ObjectId("50906d7fa3c412bb040eb591") }).student_id
6
Se ha buscado el estudiante al que pertenece el documento con UUID "50906d7fa3c412bb040eb591" y se ha encontrado que tiene el "student_id" 6.
```
Este README documenta las operaciones realizadas en la base de datos "grades" y sus resultados.
