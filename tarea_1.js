> db.grades.count()
800
> db.grades.find({"student_id": 4})
{ "_id" : ObjectId("50906d7fa3c412bb040eb588"), "student_id" : 4, "type" : "quiz", "score" : 27.29006335059361 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb587"), "student_id" : 4, "type" : "exam", "score" : 87.89071881934647 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb589"), "student_id" : 4, "type" : "homework", "score" : 5.244452510818443 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb58a"), "student_id" : 4, "type" : "homework", "score" : 28.656451042441 }
> db.grades.countDocuments({ type: "exam" })
200
> db.grades.countDocuments({ type: "homework" })
400
> db.grades.countDocuments({ type: "quiz" })
200
> db.grades.deleteMany({student_id: 3})
{ "acknowledged" : true, "deletedCount" : 4 }
> db.grades.find({score: 75.29561445722392})
{ "_id" : ObjectId("50906d7fa3c412bb040eb59e"), "student_id" : 9, "type" : "homework", "score" : 75.29561445722392 }
> db.grades.updateOne({ "_id": ObjectId("50906d7fa3c412bb040eb591") }, { $set: { "score": 100 } })
{ "acknowledged" : true, "matchedCount" : 1, "modifiedCount" : 1 }
> db.grades.findOne({ "_id": ObjectId("50906d7fa3c412bb040eb591") }).student_id
6
